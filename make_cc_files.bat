@if not exist generated_proto mkdir generated_proto
protoc.exe --proto_path=src --proto_path=demoinfogo_windows_build/include --cpp_out=generated_proto src/google/protobuf/descriptor.proto
protoc.exe --proto_path=src --proto_path=demoinfogo_windows_build/include --cpp_out=generated_proto src/netmessages.proto
protoc.exe --proto_path=src --proto_path=demoinfogo_windows_build/include --cpp_out=generated_proto src/steammessages.proto
protoc.exe --proto_path=src --proto_path=demoinfogo_windows_build/include --cpp_out=generated_proto src/cstrike15_usermessages.proto
protoc.exe --proto_path=src --proto_path=demoinfogo_windows_build/include --cpp_out=generated_proto src/cstrike15_gcmessages.proto
